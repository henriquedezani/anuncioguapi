import 'dart:convert';

import 'package:anuncio_guapi/constantes.dart';
import 'package:anuncio_guapi/models/anuncio.dart';
import 'package:anuncio_guapi/models/anunciogaleria.dart' as galeria;
import 'package:anuncio_guapi/requests/anunciorequest.dart';
import 'package:flutter/material.dart';

class AnuncioGaleria extends StatefulWidget{

  final DTOAnuncio anuncio;

  AnuncioGaleria({Key key, @required this.anuncio}):super(key:key);

  @override
  State<StatefulWidget> createState() => new AnuncioGaleriaState();
}

class AnuncioGaleriaState extends State<AnuncioGaleria>{

  final AnuncioRequest anuncioRequest = new AnuncioRequest();
  
  PageController controller;
  int currentpage = 0;

  @override
  AnuncioGaleria get widget => super.widget;

  @override
  initState() {
    super.initState();
    controller = new PageController(
      initialPage: currentpage,
      keepPage: false,
      viewportFraction: 0.5,
    );
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  void _clickNavigationBar(int idx, BuildContext ctx){
    Scaffold.of(ctx).showSnackBar(
      SnackBar(
        content: Text("Clicou em:"+ idx.toString())
      )
    );
  }

  Builder retornaBottomNavigationBar(){
    BottomNavigationBarItem anuncio = new BottomNavigationBarItem(
      icon: Icon(Icons.photo_album),
      title: Text("Anúncio"),

    );

    BottomNavigationBarItem empresa = new BottomNavigationBarItem(
      icon: Icon(Icons.business),
      title: Text("Empresa")
    );

    List<BottomNavigationBarItem> itensMenu = new List<BottomNavigationBarItem>();
    itensMenu.add(anuncio);
    itensMenu.add(empresa);

    return Builder(
      builder: (buildContext) {
        return BottomNavigationBar(
          items: itensMenu,
          currentIndex: 0,
          type: BottomNavigationBarType.fixed,
          onTap: (idx) {
            _clickNavigationBar(idx, buildContext);
          },
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("Anúncios Guapi - Galeria de Anúncio")
      ),
      bottomNavigationBar: retornaBottomNavigationBar(),
      body: new Center(
        child: new Container(
          child: FutureBuilder(
            future: anuncioRequest.getGaleria(widget.anuncio.EmpresaId, widget.anuncio.AnuncioId),
            builder: (context, snapshot){
              if (snapshot.hasData){
                List lista = json.decode(snapshot.data.body.toString());
                return PageView.builder(
                  onPageChanged: (value) {
                    setState(() {
                      currentpage = value;
                      Scaffold.of(context).showSnackBar(
                        SnackBar(
                          content: Text("Clicou em:"+ value.toString())
                        )
                      );
                    });
                  },
                  controller: controller,
                  itemCount: lista.length,
                  itemBuilder: (context, index) { 
                    var item = lista[index];
                    galeria.AnuncioGaleria anuncioGaleria = new galeria.AnuncioGaleria();
                    anuncioGaleria.Imagem = item['Imagem'];
                    anuncioGaleria.Titulo = item['Titulo'];
                    return builder(index, anuncioGaleria);
                  }
                );
              } else if (snapshot.hasError){
                return Text(snapshot.error);
              }
              return CircularProgressIndicator();
            },
          )
        )
      )
    );
  }

  builder(int index, galeria.AnuncioGaleria anuncio) {
    return new AnimatedBuilder(
      animation: controller,
      builder: (context, child) {
        double value = 1.0;
        if (controller.position.haveDimensions) {
          value = controller.page - index;
          value = (1 - (value.abs() * .5)).clamp(0.0, 1.0);
        }

        return new Center(
          child: new SizedBox(
            height: Curves.easeOut.transform(value) * 300 + 200,
            width: Curves.easeOut.transform(value) * 250,
            child: Column(
              children: [
                Image.network(
                  Constantes.Url + anuncio.Imagem,
                  height: 300.0
                ),
                Text(anuncio.Titulo)
              ]
            ),
          ),
        );
      },
      child: new Container(
        margin: const EdgeInsets.all(8.0),
        color: index % 2 == 0 ? Colors.blue : Colors.red,
      ),
    );
  }
}
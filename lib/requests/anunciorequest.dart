import 'package:anuncio_guapi/constantes.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class AnuncioRequest{
  final String URLController = Constantes.APIUrl + "anuncio/";

  Future<http.Response> getAnuncios(int empresaId) async{
    String url = URLController + "Read?empresaId=" + empresaId.toString();
    return await http.get(url);
  }

  Future<http.Response> getAnunciosSegmento(int segmentoId) async{
    String url = URLController + "BySegmento?segmentoId=" + segmentoId.toString();
    return await http.get(url);
  }

  Future<http.Response> getGaleria(int empresaId, int anuncioId) async {
    String url = URLController + "GetGallery?anuncioId="+anuncioId.toString() + "&empresaId="+empresaId.toString();
    return await http.get(url);
  }
}
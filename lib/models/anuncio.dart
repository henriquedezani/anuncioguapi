class DTOAnuncio{
  int AnuncioId;
  int EmpresaId;
  String Titulo;
  String Descricao;
  String Imagem;
  DateTime DataCriacao;
  int QtdLike;
  int QtdUnlike;

  //Propriedade virtual, não existente no banco
  String NomeEmpresa;
  bool PossuiGaleria;
}